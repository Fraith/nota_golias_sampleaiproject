function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position.",
		parameterDefs = {
			{
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{
				name = "listOfUnits", -- relative formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end


-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit


function Run(self, units, parameter)
	local listOfUnits = parameter.listOfUnits -- table
	local position = parameter.position -- Vec3
	
	if #listOfUnits <= 0 then
		return SUCCESS
	end
	
	-- pick the spring command implementing the move
	local cmdID = CMD.MOVE

	local pointX, pointY, pointZ = SpringGetUnitPosition(listOfUnits[1])
	if #listOfUnits > 0 and pointX ~= nil and pointZ ~= nil then
		
		local pointmanPosition = Vec3(pointX, pointY, pointZ)
		local difX = pointX - position.x
		local difZ = pointZ - position.z
		local squaredist = difX * difX + difZ * difZ
		if squaredist < 1250 then
			return SUCCESS
		end

		for i=1,#listOfUnits do
			SpringGiveOrderToUnit(listOfUnits[i],cmdID,position:AsSpringVector(),{})
		end

		return RUNNING
	else
		return FAILURE
	end
end

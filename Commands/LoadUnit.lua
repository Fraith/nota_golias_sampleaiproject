function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Loads target unit into transporter.",
		parameterDefs = {
			{
				name = "transporter",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{
				name = "target",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            },
		}
	}
end


-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringValidUnitID = Spring.ValidUnitID
local SpringGetUnitTransporter = Spring.GetUnitTransporter
local SpringGetUnitIsTransporting = Spring.GetUnitIsTransporting


function Run(self, units, parameter)
	local transporter = parameter.transporter -- unitID
    local target = parameter.target -- unitID

    if transporter == nil then
        return FAILURE
    end
    if target == nil then
        return SUCCESS
    end

    local validtransporter = SpringValidUnitID(transporter)
    local validtarget = SpringValidUnitID(target)

    if (validtransporter == false or validtarget == false) then
        return FAILURE
    end

    local transported = SpringGetUnitIsTransporting(transporter)
    local carrier = SpringGetUnitTransporter(target)

    if (carrier ~= nil) then
        if (carrier == transporter) then
            return SUCCESS
        else
            return FAILURE
        end
    end
	
	-- pick the spring command implementing the move
	local cmdID = CMD.LOAD_UNITS
            
    SpringGiveOrderToUnit(transporter,cmdID,{target},{})

    return RUNNING
end

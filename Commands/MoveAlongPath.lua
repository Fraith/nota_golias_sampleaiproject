function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Moves along the path provided as a list of positions to the last one.",
		parameterDefs = {
			{
				name = "unit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{
				name = "path",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            },
		}
	}
end


-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringValidUnitID = Spring.ValidUnitID

function Remove(array, idx)
    for i=idx,#array-1 do
        array[i] = array[i+1]
    end
    array[#array] = nil
end

function Run(self, units, parameter)
	local unit = parameter.unit -- unitID
    local path = parameter.path -- Array of vec3

    if unit == nil then
        return FAILURE
    end
    local validunit = SpringValidUnitID(unit)
    if (validunit == false) then
        return FAILURE
    end

    if path == nil or #path <= 0 then
        return SUCCESS
    end

    local current_target = path[#path]

    local pX, pY, pZ = SpringGetUnitPosition(unit)

    local difX = pX - current_target.x
    local difY = pY - current_target.y
    local difZ = pZ - current_target.z
    local sqrdist = difX * difX + difZ * difZ -- + difY * difY

    if sqrdist <= 150 * 150 then
        return SUCCESS
    end
	
	-- pick the spring command implementing the move
	local cmdID = CMD.MOVE
            
    local cq = Spring.GetUnitCommands(unit)
    if cq == nil or #cq <= 0 then
        for i=1,#path do
            SpringGiveOrderToUnit(unit,cmdID,path[i]:AsSpringVector(),{"shift"})
        end
    end

    return RUNNING
end

function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Unloads units from bear in specified location.",
		parameterDefs = {
			{
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{
				name = "radius",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            },
            {
				name = "bear",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end


-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit


function Run(self, units, parameter)
	local bear = parameter.bear -- table
    local position = parameter.position -- Vec3
    local radius = parameter.radius

    if bear == nil then
        return FAILURE
    end

    local units = Spring.GetUnitIsTransporting(bear)
    if units == nil then
        return SUCCESS
    end
    if #units <= 0 then
        return SUCCESS
    end
    --Spring.Echo(units)
	
	-- pick the spring command implementing the move
	local cmdID = CMD.UNLOAD_UNITS
            
    SpringGiveOrderToUnit(bear,cmdID,{position.x, position.y, position.z, radius},{})

    return RUNNING
end

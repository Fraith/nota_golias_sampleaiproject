-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

function getInfo()
	return {
		period = 0 -- no caching
	}
end

function Run(self, units, parameter)
	message.SendRules({
		subject = "manualMissionEnd",
		data = {},
    })
    return SUCCESS
end

function Reset(self)
	return self
end
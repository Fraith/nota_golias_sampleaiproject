Repository of AI projects for notA
====

*(nothing is as it seems)*

Sandsail beahviour
----
I renamed the behaviour into SandSnail because initial iteration
moved with such speed.
The iteration which never stops is not as snail-ish but that doesn't matter.

CaptureHills behaviour
----
Upon selecting units in ctp2 and issuing the command, behaviour attempts to
capture the 4 hills on the map by sending pws to safe hills and transporting warriors
to the dangerous hill.

TTDR behaviour
----
Ther order issues commands to transporters and scouts, scouts move out of the way
and transporters start collecting units from around the map using simple path finding.
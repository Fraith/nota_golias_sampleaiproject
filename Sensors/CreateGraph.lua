local sensorInfo = {
	name = "createGraph",
	desc = "Creates Graph with respect to enemies.",
	author = "Fraith",
	date = "2019-05-06",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetGroundHeight = Spring.GetGroundHeight

function IsInDanger(position, danger)
    for i=1,#danger do
        local dist = position:Distance(danger[i].position)
        if dist <= danger[i].radius then
            return true
        end
    end

    return false
end

-- @description returns generated graph
return function(danger)

    local mapsizeX = Game.mapSizeX
    local mapsizeZ = Game.mapSizeZ
    local step = 100
    local countx = math.floor(mapsizeX / step)
    local countz = math.floor(mapsizeZ / step)

    local graph = {
        nodes = {},
        vertices = {},
        neighbours = {}
    }

    local ntest = {}

    for xi=1,countx do
        for zi=1,countz do
            local cX = (xi - 0.8) * step
            local cZ = (zi - 0.9) * step
            local height = SpringGetGroundHeight(cX, cZ)
            local pos = Vec3(cX, height, cZ)
            if (IsInDanger(pos, danger) == false) then
                local idx = (xi - 1) * countz + zi
                graph.nodes[#graph.nodes + 1] = idx
                graph.vertices[idx] = pos
                graph.neighbours[idx] = {}
                ntest[idx] = {}
                ntest[idx][1] = { i = idx - 1, x = xi, z = zi - 1 }
                ntest[idx][2] = { i = idx + countz, x = xi + 1, z = zi }
                ntest[idx][3] = { i = idx + 1, x = xi, z = zi + 1 }
                ntest[idx][4] = { i = idx - countz, x = xi - 1, z = zi }
                --alternative 8 direction moving - caused some pathing issues
                --ntest[idx][1] = { i = idx - countz - 1, x = xi - 1, z = zi - 1 }
                --ntest[idx][2] = { i = idx - countz, x = xi - 1, z = zi }
                --ntest[idx][3] = { i = idx - countz + 1, x = xi - 1, z = zi + 1 }
                --ntest[idx][4] = { i = idx - 1, x = xi, z = zi - 1 }
                --ntest[idx][5] = { i = idx + 1, x = xi, z = zi + 1 }
                --ntest[idx][6] = { i = idx + countz - 1, x = xi + 1, z = zi - 1 }
                --ntest[idx][7] = { i = idx + countz, x = xi + 1, z = zi }
                --ntest[idx][8] = { i = idx + countz + 1, x = xi + 1, z = zi + 1 }
            end
        end
    end

    for i=1,#graph.nodes do
        local idx = graph.nodes[i]
        local n = ntest[idx]
        for j=1,#n do
            if graph.vertices[n[j].i] ~= nil and n[j].x > 0 and n[j].z > 0 and n[j].x <= countx and n[j].z <= countz then
                graph.neighbours[idx][#graph.neighbours[idx] + 1] = n[j].i
            end
        end
    end

    return graph
end
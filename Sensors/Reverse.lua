local sensorInfo = {
	name = "reverse",
	desc = "Flips array.",
	author = "Fraith",
	date = "2019-05-09",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups

-- @description returns flipped array
return function(array)

    local path = {}
    for i=#array,1,-1 do
        path[#path + 1] = array[i]
    end
    
    return path
end
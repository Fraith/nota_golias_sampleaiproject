local sensorInfo = {
	name = "unitsMedianPosition",
	desc = "Finds the median position of a group of units.",
	author = "Fraith",
	date = "2019-04-27",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition

-- @description return current wind statistics
return function(listOfUnits)
    local posX = 0;
    local posY = 0;
    local posZ = 0;
    for i = 1,#listOfUnits do
        local pointX, pointY, pointZ = SpringGetUnitPosition(listOfUnits[i])
        posX = posX + pointX
        posY = posY + pointY
        posZ = posZ + pointZ
    end
    posX = posX / #listOfUnits
    posY = posY / #listOfUnits
    posZ = posZ / #listOfUnits

    return Vec3(posX, posY, posZ)
end
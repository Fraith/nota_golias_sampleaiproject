local sensorInfo = {
	name = "windChanged",
	desc = "Checks wether the wind direction changed significantly.",
	author = "Fraith",
	date = "2019-04-27",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetWind = Spring.GetWind

-- @description return current wind statistics
return function(oldwind)
    if oldwind == nil then
        return true --If there is no reference wind then it changed
    end
	local dirX, dirY, dirZ, strength, normDirX, normDirY, normDirZ = SpringGetWind()
    local oldX = oldwind.normDirX
    local oldZ = oldwind.normDirZ
    local difX = oldX - normDirX
    local difZ = oldZ - normDirZ
    if difX < 0 then
        difX = -difX
    end
    if difZ < 0 then
        difZ = -difZ
    end
    if difX > 0.1 or difZ > 0.1 then
        return true
    end
    return false
end
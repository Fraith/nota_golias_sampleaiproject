local sensorInfo = {
	name = "getAtlas",
	desc = "Selects free atlas.",
	author = "Fraith",
	date = "2019-05-06",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition

-- @description return current wind statistics
return function()
    
    local units = Sensors.core.EnemyUnits()
    

    local defid = Spring.GetUnitDefID(units[1])

    local def = UnitDefs[defid]

    --local weps = def.weapons

    --local info = WeaponDefs[weps[1].weaponDef]

    --return {
    --    un = def.name,
    --    wn = info.name,
    --    range = info.range,
    --}
    return defid
end
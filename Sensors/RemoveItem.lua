local sensorInfo = {
	name = "removeItem",
	desc = "Removes item from array.",
	author = "Fraith",
	date = "2019-05-06",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition

-- @description return current wind statistics
return function(array, item)

    local newar = {}
    for i=1,#array do
        if array[i] ~= item then
            newar[#newar + 1] = array[i]
        end
    end

    return newar
end
local sensorInfo = {
	name = "getUnusedLocation",
	desc = "Gets unused location.",
	author = "Fraith",
	date = "2019-05-06",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups

-- @description returns location which has not been used so far
return function(locations, usedlocs)
    for i=1,#locations do
        local l = locations[i]
        if usedlocs[i] == nil then
            return i
        end
    end

    return math.floor(#locations / 2)
end
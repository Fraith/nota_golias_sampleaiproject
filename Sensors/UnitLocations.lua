local sensorInfo = {
	name = "unitLocations",
	desc = "Returns array of locations of the given units.",
	author = "Fraith",
	date = "2019-05-06",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition

-- @description return current wind statistics
return function(listOfUnits)
    local locs = {}

    for i=1,#listOfUnits do
        local thisUnitID = listOfUnits[i]
        local pointX, pointY, pointZ = SpringGetUnitPosition(listOfUnits[i])
        locs[i] = Vec3(pointX, pointY, pointZ)
    end

    return locs
end
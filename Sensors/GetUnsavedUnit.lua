local sensorInfo = {
	name = "getUnsavedUnit",
	desc = "Gets unit which has not been saved so far.",
	author = "Fraith",
	date = "2019-05-06",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringValidUnitID = Spring.ValidUnitID
local SpringGetUnitTransporter = Spring.GetUnitTransporter

-- @description returns unit which is outside of the safe zone and is not targeted by any transporter, prefers units which didn't get rescue attempt
return function(units, transport, attempt)
    local mi = Sensors.core.MissionInfo()
    local pos = mi.safeArea.center
    local r = mi.safeArea.radius
    for i=1,#units do
        local u = units[i]
        local v = SpringValidUnitID(u)
        if v then
            local t SpringGetUnitTransporter(u)
            local pt = transport[u]
            local pX, pY, pZ = SpringGetUnitPosition(u)
            local p = Vec3(pX, pY, pZ)
            if p:Distance(pos) >= r then
                if attempt[u] == nil then
                    attempt[u] = true
                    return u
                end
                if pt == nil then
                    return u
                end
                local vpt = SpringValidUnitID(pt)
                if vpt == false then
                    return u
                end
            end
        end
    end

    return nil
end
local sensorInfo = {
	name = "splitLocationsByDanger",
	desc = "Separates dangerous location from the given list according to mission info.",
	author = "Fraith",
	date = "2019-04-27",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups

-- @description return current wind statistics
return function(listOfLocs, mi, radius)
    local eloc = mi.enemyPositions[1]
    local idx = 1
    for i=1,#listOfLocs do
        local loc = listOfLocs[i]
        local difX = loc.x - eloc.x
        local difY = loc.y - eloc.y
        local difZ = loc.z - eloc.z
        local squaredist = difX * difX + difY * difY + difZ * difZ
        if squaredist <= radius * radius then
            idx = i
        end
    end
    local safelocs = {}
    local j = 1
    for i=1,#listOfLocs do
        if i ~= idx then
            safelocs[j] = listOfLocs[i]
            j = j + 1
        end
    end
    return {
        danger = listOfLocs[idx],
        safe = safelocs
    }
end
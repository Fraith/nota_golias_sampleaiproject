local sensorInfo = {
	name = "updateAfterTransport",
	desc = "Updates the used tables after actual transportation happens.",
	author = "Fraith",
	date = "2019-05-06",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition

-- @description returns update used table
return function(used, transporter, target, safeidx)
    used.transport[transporter] = nil
    used.transport[target] = nil
    used.saved[target] = true

    return used
end
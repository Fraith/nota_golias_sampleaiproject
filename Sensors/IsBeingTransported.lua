local sensorInfo = {
	name = "isBeingTransported",
	desc = "Checks wether given unit is being transported.",
	author = "Fraith",
	date = "2019-05-07",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition

-- @description Checks wether given unit is being transported
return function(unit)
    
    local transp = Spring.GetUnitTransporter(unit)
    if transp == nil then
        return false
    end

    return true
end
local sensorInfo = {
	name = "getClosestPoint",
	desc = "Returns the index of the closest point in a table.",
	author = "Fraith",
	date = "2019-05-06",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition

-- @description returns the closes point from points, table indices are given in nodes parameter
return function(position, points, nodes)

    local mindist = position:Distance(points[nodes[1]])
    local idx = nodes[1]
    for i=2,#nodes do
        local dist = position:Distance(points[nodes[i]])
        if dist < mindist then
            mindist = dist
            idx = nodes[i]
        end
    end

    return idx
end
local sensorInfo = {
	name = "getDangerousAreas",
	desc = "Returns an array of dangerous areas.",
	author = "Fraith",
	date = "2019-05-09",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition

-- @description returns an array of circular areas considered as dangerous because of enemies + on supplied positions
return function(other)

    local enemies = Sensors.core.EnemyUnits()
    local danger = {}

    for i=1,#enemies do
        local enemyID = enemies[i]
        local pointX, pointY, pointZ = SpringGetUnitPosition(enemyID)
        danger[i] = {
            position = Vec3(pointX, pointY, pointZ),
            radius = 750,
        }
    end
    for i=1,#other do
        danger[#danger + 1] = {
            position = other[i],
            radius = 800
        }
    end

    return danger
end
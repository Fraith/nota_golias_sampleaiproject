local sensorInfo = {
	name = "isTransporting",
	desc = "Checks wether given unit is transporting something.",
	author = "Fraith",
	date = "2019-05-07",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition

-- @description Checks wether given unit is transporting something
return function(unit)
    
    local transp = Spring.GetUnitIsTransporting(unit)
    if transp == nil then
        return false
    end
    if #transp <= 0 then
        return false
    end

    return true
end
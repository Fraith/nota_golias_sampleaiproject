local sensorInfo = {
	name = "getAllAllyUnits",
	desc = "Gets all ally units on the map.",
	author = "Fraith",
	date = "2019-05-06",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups

-- @description returns all ally units on the map
return function()
    local units = Spring.GetAllUnits()
    local myunits = {}
    for i=1,#units do
        if Spring.IsUnitAllied(units[i]) then
            myunits[#myunits + 1] = units[i]
        end
    end

    return myunits
end
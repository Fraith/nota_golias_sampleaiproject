local sensorInfo = {
	name = "getUnusedTransporter",
	desc = "Gets unused transporter.",
	author = "Fraith",
	date = "2019-05-06",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups

-- @description returns transporter that is currently idle
return function(transporters, transport)
    for i=1,#transporters do
        local t = transporters[i]
        if transport[t] == nil then
            return t
        end
    end

    return nil
end
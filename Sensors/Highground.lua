local sensorInfo = {
	name = "highgroundPoints",
	desc = "Return points on the map considered as highground - median point for each hill.",
	author = "Fraith",
	date = "2019-04-28",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function medianPoint(listOfPositions)
    local posX = 0;
    local posY = 0;
    local posZ = 0;
    for i = 1,#listOfPositions do
        local pos = listOfPositions[i]
        posX = posX + pos.x
        posY = posY + pos.y
        posZ = posZ + pos.z
    end
    posX = posX / #listOfPositions
    posY = posY / #listOfPositions
    posZ = posZ / #listOfPositions

    return Vec3(posX, posY, posZ)
end

-- speedups
local SpringGetGroundHeight = Spring.GetGroundHeight

-- @description return current wind statistics
return function(treshold, mapstep, mapdist)
    local mapsizeX = Game.mapSizeX
    local mapsizeZ = Game.mapSizeZ
    local step = mapstep
    local dist = mapdist

    local points = {}
    -- Step through the map
    for i=0,mapsizeX,step do
        for j=0,mapsizeZ,step do
            local height = SpringGetGroundHeight(i, j)
            -- Check if the map point is above treshold
            if height > treshold then
                local p = Vec3(i, height, j)
                -- Solve edge case of empty list
                if #points <= 0 then
                    points[1] = { p }
                else
                    local added = false
                    -- Check the distance from the first point in each group
                    -- If its lower than provided distance add it to the list
                    for k=1,#points do
                        local vec = p - points[k][1]
                        if vec:LengthSqr() <= dist * dist then
                            points[k][#points[k] + 1] = p
                            added = true
                            break
                        end
                    end
                    -- Point didn't belong to any list, add new list for it
                    if added == false then
                        points[#points + 1] = { p }
                    end
                end
            end
        end
    end
    -- We want median point of each list of points
    local newpoints = {}
    for i=1,#points do       
        newpoints[i] = medianPoint(points[i])
    end
    return newpoints
end
local sensorInfo = {
	name = "findPath",
	desc = "Returns path in graph between two positions.",
	author = "Fraith",
	date = "2019-05-09",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition

function Remove(array, idx)
    for i=idx,#array-1 do
        array[i] = array[i+1]
    end
    array[#array] = nil
end

function IsInArray(array, item)
    for i=1,#array do
        if array[i] == item then
            return true
        end
    end
    return false
end

-- @description returns path between startp and endp from graph
return function(graph, startp, endp)

    local path = {}
    local startidx = Sensors.nota_golias_sampleaiproject.GetClosestPoint(startp, graph.vertices, graph.nodes)
    local endidx = Sensors.nota_golias_sampleaiproject.GetClosestPoint(endp, graph.vertices, graph.nodes)

    local q = { startidx }
    local visited = {}
    local parents = {}

    while #q > 0 do
        local cidx = q[1]
        Remove(q, 1)
        if (cidx == endidx) then
            break;
        end

        visited[cidx] = true
        local n = graph.neighbours[cidx]
        for i=1,#n do
            if visited[n[i]] == nil and IsInArray(q, n[i]) == false then
                q[#q + 1] = n[i]
                if parents[n[i]] == nil then
                    parents[n[i]] = cidx
                end
            end
        end
    end

    local point = endidx
    local rpath = {}
    while parents[point] ~= nil do
        rpath[#rpath + 1] = graph.vertices[point]
        point = parents[point]
    end

    for i=#rpath,1,-1 do
        path[#path + 1] = rpath[i]
    end

    path[#path + 1] = endp
    return path
end
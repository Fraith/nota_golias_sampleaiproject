local sensorInfo = {
	name = "unitLocation",
	desc = "Returns the location of the unit.",
	author = "Fraith",
	date = "2019-05-06",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition

-- @description returns location of specified unit
return function(unit)

    local pointX, pointY, pointZ = SpringGetUnitPosition(unit)

    return Vec3(pointX, pointY, pointZ)
end
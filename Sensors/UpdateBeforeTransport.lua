local sensorInfo = {
	name = "updateBeforeTransport",
	desc = "Updates the used tables before actual transportation happens.",
	author = "Fraith",
	date = "2019-05-06",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition

-- @description returns updated used table
return function(used, transporter, target, safeidx)
    used.transport[transporter] = target
    used.transport[target] = transporter
    used.usedlocs[safeidx] = true

    return used
end
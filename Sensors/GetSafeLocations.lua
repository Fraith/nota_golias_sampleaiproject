local sensorInfo = {
	name = "getSafeLocations",
	desc = "Gets spaced safe location in a given circle.",
	author = "Fraith",
	date = "2019-05-08",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups

-- @description return current wind statistics
return function(position, radius, space)
    local locs = {}

    local count = math.floor(2 * radius / space) - 1
    local topleftX = position.x - radius + space / 2
    local topleftZ = position.z - radius + space / 2

    for i=1,count do
        for j=1,count do
            local cZ = topleftZ + (i - 1) * space
            local cX = topleftX + (j - 1) * space
            local cY = Spring.GetGroundHeight(cX,cZ)

            local difX = cX - position.x
            local difZ = cZ - position.z

            local sqrdist = difX * difX + difZ * difZ

            if (sqrdist < radius * radius) then
                locs[#locs + 1] = Vec3(cX, cY, cZ)
            end
        end
    end

    return locs
end
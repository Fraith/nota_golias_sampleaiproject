local sensorInfo = {
	name = "checkMissionEnd",
	desc = "Checks wether ttdr mission is finished.",
	author = "Fraith",
	date = "2019-05-06",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringValidUnitID = Spring.ValidUnitID

-- @description returns wether ttdr is finished - enough points, no transporters, no units to save
return function(mi, transporters, unitstosave)

    local alldead = true
    for i=1,#transporters do
        if SpringValidUnitID(transporters[i]) then
            alldead = false
        end
    end
    if alldead then
        return true
    end
    if mi.score >= mi.scoreForBonus then
        return true
    end
    local allsaved = true
    for i=1,#unitstosave do
        local u = unitstosave[i]
        local v = SpringValidUnitID(u)
        if v then
            local pX, pY, pZ = SpringGetUnitPosition(u)
            local p = Vec3(pX, pY, pZ)
            if p:Distance(mi.safeArea.center) >= mi.safeArea.radius then
                allsaved = false
            end
        end
    end
    if allsaved then
        return true
    end

    return false
end